#include "FreeRTOS.h"
#include "task.h"
#include "gpio.h"
#include "taskManager.h"
#include "link_log.h"


/*FreeRTOS配置*/

/* START_TASK 任务 配置
 * 包括: 任务句柄 任务优先级 堆栈大小 创建任务
 */
#define START_TASK_PRIO 1                   /* 任务优先级 */
#define START_STK_SIZE  128                 /* 任务堆栈大小 */
TaskHandle_t            StartTask_Handler;  /* 任务句柄 */
void start_task(void *pvParameters);        /* 任务函数 */




void freertos_demo(void)
{    
    xTaskCreate((TaskFunction_t )start_task,            /* 任务函数 */
                (const char*    )"start_task",          /* 任务名称 */
                (uint16_t       )START_STK_SIZE,        /* 任务堆栈大小 */
                (void*          )NULL,                  /* 传入给任务函数的参数 */
                (UBaseType_t    )START_TASK_PRIO,       /* 任务优先级 */
                (TaskHandle_t*  )&StartTask_Handler);   /* 任务句柄 */
    vTaskStartScheduler();
}

extern void send_it_test(void);
static void start_task(void *pvParameters)
{

//	taskENTER_CRITICAL();
//	osThreadTerminate(defaultTaskHandle);
//	taskEXIT_CRITICAL();
	
	
//	uint32_t time = 0;
	
	while (1)
	{
		HAL_GPIO_TogglePin(LED_GPIO_Port, LED_Pin);
		vTaskDelay(500);  /* 延时1000ticks */
//		send_it_test();
//		LINK_LOG_DEBUG("led toggle!-->time:%d\r\n", time++);
	}
	
//	os_run();
}


task_define("startTask", start_task, 128, 10);
