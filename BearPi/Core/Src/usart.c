/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file    usart.c
  * @brief   This file provides code for the configuration
  *          of the USART instances.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2024 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "usart.h"

/* USER CODE BEGIN 0 */
#include <stdio.h>
#include "ringbuffer.h"
#include "os_port.h"
#include "semphr.h"



#define CN_RCV_RING_BUFLEN  128

#define MODULE_RXBUF_SIZE  256
//#define MODULE_TXBUF_SIZE  1024




static unsigned char      s_uartdebug_rcv_ringmem[CN_RCV_RING_BUFLEN];
static ring_buf_t  s_uartdebug_rcv_ring;

									  
static uint8_t uart1_rx_ch;
static uint8_t lpuart1_rx_ch;

static unsigned char mrxbuf[MODULE_RXBUF_SIZE];   /*接收缓冲区 ------------*/
//static unsigned char mtxbuf[MODULE_TXBUF_SIZE];   /*发送缓冲区 ------------*/

static ring_buf_t mrbrecv;               /*收发缓冲区管理 --------*/


//os_sem_t uartdebug_rcv_sync;

/* USER CODE END 0 */

UART_HandleTypeDef hlpuart1;
UART_HandleTypeDef huart1;

/* LPUART1 init function */

void MX_LPUART1_UART_Init(void)
{

  /* USER CODE BEGIN LPUART1_Init 0 */

  /* USER CODE END LPUART1_Init 0 */

  /* USER CODE BEGIN LPUART1_Init 1 */

  /* USER CODE END LPUART1_Init 1 */
  hlpuart1.Instance = LPUART1;
  hlpuart1.Init.BaudRate = 9600;
  hlpuart1.Init.WordLength = UART_WORDLENGTH_8B;
  hlpuart1.Init.StopBits = UART_STOPBITS_1;
  hlpuart1.Init.Parity = UART_PARITY_NONE;
  hlpuart1.Init.Mode = UART_MODE_TX_RX;
  hlpuart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  hlpuart1.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  hlpuart1.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&hlpuart1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN LPUART1_Init 2 */
    ring_buf_init(&mrbrecv, mrxbuf, sizeof(mrxbuf));
//	ring_buf_init(&mrbsend, mtxbuf, sizeof(mtxbuf));
	HAL_UART_Receive_IT(&hlpuart1, &lpuart1_rx_ch, 1);
  /* USER CODE END LPUART1_Init 2 */

}
/* USART1 init function */

void MX_USART1_UART_Init(void)
{

  /* USER CODE BEGIN USART1_Init 0 */

  /* USER CODE END USART1_Init 0 */

  /* USER CODE BEGIN USART1_Init 1 */

  /* USER CODE END USART1_Init 1 */
  huart1.Instance = USART1;
  huart1.Init.BaudRate = 115200;
  huart1.Init.WordLength = UART_WORDLENGTH_8B;
  huart1.Init.StopBits = UART_STOPBITS_1;
  huart1.Init.Parity = UART_PARITY_NONE;
  huart1.Init.Mode = UART_MODE_TX_RX;
  huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart1.Init.OverSampling = UART_OVERSAMPLING_16;
  huart1.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart1.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART1_Init 2 */
	ring_buf_init(&s_uartdebug_rcv_ring, s_uartdebug_rcv_ringmem, sizeof(s_uartdebug_rcv_ringmem));
	HAL_UART_Receive_IT(&huart1, &uart1_rx_ch, 1);
  /* USER CODE END USART1_Init 2 */

}

void HAL_UART_MspInit(UART_HandleTypeDef* uartHandle)
{

  GPIO_InitTypeDef GPIO_InitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};
  if(uartHandle->Instance==LPUART1)
  {
  /* USER CODE BEGIN LPUART1_MspInit 0 */

  /* USER CODE END LPUART1_MspInit 0 */

  /** Initializes the peripherals clock
  */
    PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_LPUART1;
    PeriphClkInit.Lpuart1ClockSelection = RCC_LPUART1CLKSOURCE_PCLK1;
    if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
    {
      Error_Handler();
    }

    /* LPUART1 clock enable */
    __HAL_RCC_LPUART1_CLK_ENABLE();

    __HAL_RCC_GPIOC_CLK_ENABLE();
    /**LPUART1 GPIO Configuration
    PC0     ------> LPUART1_RX
    PC1     ------> LPUART1_TX
    */
    GPIO_InitStruct.Pin = WAN_UART_RX_Pin|WAN_UART_TX_Pin;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
    GPIO_InitStruct.Alternate = GPIO_AF8_LPUART1;
    HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

    /* LPUART1 interrupt Init */
    HAL_NVIC_SetPriority(LPUART1_IRQn, 5, 0);
    HAL_NVIC_EnableIRQ(LPUART1_IRQn);
  /* USER CODE BEGIN LPUART1_MspInit 1 */

  /* USER CODE END LPUART1_MspInit 1 */
  }
  else if(uartHandle->Instance==USART1)
  {
  /* USER CODE BEGIN USART1_MspInit 0 */

  /* USER CODE END USART1_MspInit 0 */

  /** Initializes the peripherals clock
  */
    PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USART1;
    PeriphClkInit.Usart1ClockSelection = RCC_USART1CLKSOURCE_PCLK2;
    if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
    {
      Error_Handler();
    }

    /* USART1 clock enable */
    __HAL_RCC_USART1_CLK_ENABLE();

    __HAL_RCC_GPIOA_CLK_ENABLE();
    /**USART1 GPIO Configuration
    PA9     ------> USART1_TX
    PA10     ------> USART1_RX
    */
    GPIO_InitStruct.Pin = GPIO_PIN_9|GPIO_PIN_10;
    GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
    GPIO_InitStruct.Alternate = GPIO_AF7_USART1;
    HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

    /* USART1 interrupt Init */
    HAL_NVIC_SetPriority(USART1_IRQn, 10, 0);
    HAL_NVIC_EnableIRQ(USART1_IRQn);
  /* USER CODE BEGIN USART1_MspInit 1 */
	 
//	uartdebug_rcv_sync = xSemaphoreCreateBinary();
	 
  /* USER CODE END USART1_MspInit 1 */
  }
}

void HAL_UART_MspDeInit(UART_HandleTypeDef* uartHandle)
{

  if(uartHandle->Instance==LPUART1)
  {
  /* USER CODE BEGIN LPUART1_MspDeInit 0 */

  /* USER CODE END LPUART1_MspDeInit 0 */
    /* Peripheral clock disable */
    __HAL_RCC_LPUART1_CLK_DISABLE();

    /**LPUART1 GPIO Configuration
    PC0     ------> LPUART1_RX
    PC1     ------> LPUART1_TX
    */
    HAL_GPIO_DeInit(GPIOC, WAN_UART_RX_Pin|WAN_UART_TX_Pin);

    /* LPUART1 interrupt Deinit */
    HAL_NVIC_DisableIRQ(LPUART1_IRQn);
  /* USER CODE BEGIN LPUART1_MspDeInit 1 */

  /* USER CODE END LPUART1_MspDeInit 1 */
  }
  else if(uartHandle->Instance==USART1)
  {
  /* USER CODE BEGIN USART1_MspDeInit 0 */

  /* USER CODE END USART1_MspDeInit 0 */
    /* Peripheral clock disable */
    __HAL_RCC_USART1_CLK_DISABLE();

    /**USART1 GPIO Configuration
    PA9     ------> USART1_TX
    PA10     ------> USART1_RX
    */
    HAL_GPIO_DeInit(GPIOA, GPIO_PIN_9|GPIO_PIN_10);

    /* USART1 interrupt Deinit */
    HAL_NVIC_DisableIRQ(USART1_IRQn);
  /* USER CODE BEGIN USART1_MspDeInit 1 */

  /* USER CODE END USART1_MspDeInit 1 */
  }
}

/* USER CODE BEGIN 1 */

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
	if (huart->Instance == huart1.Instance)
	{
		ring_buf_put(&s_uartdebug_rcv_ring, &uart1_rx_ch,1);
		HAL_UART_Receive_IT(huart, &uart1_rx_ch, 1);
	}
	else if (huart->Instance == hlpuart1.Instance)
	{
		ring_buf_put(&mrbrecv, &lpuart1_rx_ch,1);
		HAL_UART_Receive_IT(huart, &lpuart1_rx_ch, 1);
	}
}
#include <string.h>
void send_it_test(void)
{
	uint8_t buff[] = "send it test\r\n";
//	if (huart1.gState == HAL_UART_STATE_READY)
//	{
		HAL_UART_Transmit_IT(&huart1, buff, strlen((char *)buff));
		while(huart1.gState != HAL_UART_STATE_READY);
//	}
	
}
//void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart)
//{
//	uint8_t tx_char;
//	if (huart->Instance == huart1.Instance)
//	{
//		ring_buf_put(&s_uartdebug_rcv_ring, &uart1_rx_ch,1);
//		if (ring_buf_get(&mrbsend, &tx_char, 1) != 0)
//		{
//			HAL_UART_Transmit_IT(huart, &tx_char, 1);
//		}
//	}
//	else if (huart->Instance == hlpuart1.Instance)
//	{
//		ring_buf_put(&mrbrecv, &lpuart1_rx_ch,1);
//		HAL_UART_Receive_IT(huart, &lpuart1_rx_ch, 1);
//	}
//}

unsigned int cli_read(unsigned char *buf, unsigned int len)
{
	 return ring_buf_get(&s_uartdebug_rcv_ring, (unsigned char *)buf, len);
}
unsigned int cli_write(const unsigned char *buf, unsigned int len)
{
	HAL_StatusTypeDef status;
	
	if ((status = HAL_UART_Transmit_IT(&huart1, (uint8_t *)buf, len)) == HAL_OK)
	{
		while(huart1.gState != HAL_UART_STATE_READY);
		return len;
	}
	
	return status;
}

unsigned int wan_read(void *buf, unsigned int len)
{
	 return ring_buf_get(&mrbrecv, (unsigned char *)buf, len);
}

unsigned int wan_write(const void *buf, unsigned int len)
{	
	HAL_StatusTypeDef status;
	
//	if ((status = HAL_UART_Transmit_IT(&hlpuart1, (uint8_t *)buf, len)) == HAL_OK)
//	{
//		while(huart1.gState != HAL_UART_STATE_READY);
//		return len;
//	}
	
	status = HAL_UART_Transmit(&hlpuart1, (uint8_t *)buf, len, 0xFFFF);
	
	return status;
}


#include <stdarg.h>

#ifndef CONFIG_LINK_LOGBUF_LEN
#define CONFIG_LINK_LOGBUF_LEN      256  ///< you could modify it
#endif


void my_link_printf(const char *format, ...)
{
    char str_buf[CONFIG_LINK_LOGBUF_LEN] = {0};
    va_list list;

    (void) (void) memset(str_buf, 0, CONFIG_LINK_LOGBUF_LEN);

    va_start(list, format);
    (void) vsnprintf(str_buf, sizeof(str_buf), format, list);
    va_end(list);

    HAL_UART_Transmit_IT(&huart1, (uint8_t *)str_buf, strlen((char *)str_buf));
	while(huart1.gState != HAL_UART_STATE_READY);

}


/* define fputc */
#if defined ( __CC_ARM ) || defined ( __ICCARM__ )  /* KEIL and IAR: printf will call fputc to print */
int fputc(int ch, FILE *f)
{
    HAL_UART_Transmit(&huart1, (uint8_t *)&ch, 1, 0xFFFF);
    return ch;
}



int debug_log(char *buf, unsigned int len)
{
	return HAL_UART_Transmit(&huart1, (uint8_t *)buf, len, 0xFFFF);
}


//int fgetc(FILE *f){
//    int ret = 0;
//    unsigned char  value;
//    do
//    {
//        if(os_sem_wait(uartdebug_rcv_sync, portMAX_DELAY))
//        {
//            ret = ring_buf_get(&s_uartdebug_rcv_ring,&value,1);
//        }
//    }while(ret <=0);
//    ret = value;
//    return ret;
//}

#elif defined ( __GNUC__ )  /* GCC: printf will call _write to print */


__attribute__((used)) int _write(int fd, char *ptr, int len)
{
    if(s_uart_init)
    {
        HAL_UART_Transmit(&uart_debug, (uint8_t *)ptr, len, 0xFFFF);
    }
    return len;
}
__attribute__((used)) int _read(int fd, char *ptr, int len)
{
    int ret = 0;
    unsigned char  value;
    do
    {
        if(LOS_OK == LOS_SemPend(s_uartdebug_rcv_sync,LOS_WAIT_FOREVER))
        {
            ret = ring_buffer_read(&s_uartdebug_rcv_ring,&value,1);
        }
    }while(ret <=0);
    *(unsigned char *)ptr = value;
    return 1;
}

#endif


/* USER CODE END 1 */
