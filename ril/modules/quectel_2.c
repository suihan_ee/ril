/******************************************************************************
 * @brief    移远模组(BC35-G&BC28&BC95 R2.0)相关接口实现
 *
 * Copyright (c) 2024, <chenjunbin@163.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs: 
 * Date           Author       Notes 
 * 2024-06-12     Chjb        Initial version.
 
 ******************************************************************************/#include "ril_device.h"
#include "ril_device.h"
#include <stdarg.h>
#include <stddef.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

/* socket 收发信息 -----------------------------------------------------------*/
struct  socket_info {
	const socket_base_t *s;
	unsigned char *buf;
	int           bufsize;
	int           count;
};

static int bc28_disconnect(struct ril_device *dev, socket_base_t *s);

/**  
 * @brief       模组初始化
 */
static int bc28_init(struct ril_device *dev)
{
    const char *cmds[] = {
    "ATE0",
    "AT+QREGSWT=1",
    "AT+CMEE=1",                   
    "AT+NCONFIG=AUTOCONNECT,FALSE",
	"AT+CFUN=0",
    "AT+NBAND=5,8,20",
    "AT+CFUN=1",		
    "AT+COPS?",
    "AT+CGDCONT?",
    "AT+NCDP=221.229.214.202,5683",  // "AT+NCDP=180.101.147.115,5682","AT+NCDP=119.3.250.80,5683"
	"AT+NCDP?",
	"AT+CGATT=1",
	"AT+NNMI=1",
    NULL
    };
    return ril_send_multiline( cmds);
}

/** 
 * @brief       复位模组
 */ 
//static int bc28_reset(struct ril_device *dev)
//{
//	ril_delay(500);
//}

/** 
 * @brief       关闭模组
 */ 
static int bc28_shutdown(struct ril_device *dev)
{
//    char recv[32];
//    int ret;
//    at_respond_t resp = {"POWERED DOWN", recv, sizeof(recv), 30 * 1000};  //等待时间较长
//    ret = ril_exec_cmdx(&resp, "AT+QPOWD");
//    at_delay(2000);
//    dev->adap->pin_ctrl(RIL_PIN_POWER, 0, 0);
//    dev->adap->pin_ctrl(RIL_PIN_PWRKEY, 0, 0);
//    dev->adap->pin_ctrl(RIL_PIN_RESET, 0, 0);
//    return ret;
	
	return 0;
}

/** 
 * @brief       获取网络状态
 * @params[out] - status, 网络连接状态
 */
static int bc28_netconn_status(struct ril_device *dev, ril_netconn_status *status)
{
	int state;
	char recv[128];
	at_respond_t resp = {"OK", recv,sizeof(recv), 120 * 1000};
	if (ril_exec_cmdx(&resp, "AT+CGATT?") != RIL_OK)
		return RIL_ERROR;
	//+CGATT:<state>
	if (sscanf(recv, "%*[^+]+CGATT:%d", &state) != 1)
		return RIL_ERROR;
	*status = state ? NETCONN_ONLINE : NETCONN_OFFLINE;
	return RIL_OK;
}


/** 
 * @brief    建立PDP
 */

static int bc28_pdp_setup(struct ril_device *dev)
{
	ril_config_t *c = dev->config;
	
	/* 
     * @brief    设置APN
     */
	if ((c->apn.apn == NULL) || strlen(c->apn.apn))
		return RIL_OK;
    else
        return ril_exec_cmdx(NULL, "AT+CGDCONT=1,\"IP\",\"HUAWEI.COM\"");
}


/**  
 * @brief    网络启用/禁用控制(对应PDP激活)
 */
static int bc28_pdp_ctrl(struct ril_device *dev, bool enable)
{
//    char recv[32];
//    at_respond_t resp = {"OK", recv, sizeof(recv), 120 * 1000};  //等待时间较长
//    return ril_exec_cmdx(&resp, "AT+%s=1", enable ? "QIACT" : "QIDEACT");  
	return RIL_OK;
}

/**  
 * @brief    连接服务器
 */
static int bc28_sock_connect(struct ril_device *dev, socket_base_t *s)
{
    bool ret;
    char recv[64];
    at_respond_t resp = {"OK", recv, sizeof(recv), 120 * 1000};
//    ret = ril_exec_cmdx(&resp, "AT+QIOPEN=1,%d,%s,\"%s\",%d,%d,0",
//                        s->id, s->type == RIL_SOCK_TCP ? "\"TCP\"":"\"UDP\"", 
//                        s->host, s->port, 0);
//    if (ret != RIL_OK)
//        bc28_disconnect(dev, s);
	
	ret = ril_exec_cmdx(&resp, "AT+QLWSREGIND=0");
	if (ret != RIL_OK)
		ret = ril_exec_cmdx(&resp, "AT+QLWSREGIND=1");

    return ret;
}

/**  
 * @brief    数据发送处理
 */
static int socket_send_handler(at_work_ctx_t *e)
{
    struct socket_info *i = (struct socket_info *)e->params;
//    const socket_base_t *s    = i->s;
 
//    e->printf(e, "AT+QLWULDATA=%d,%d",s->id, i->bufsize);
//    
//    if (e->wait_resp(e, ">", 5000) != AT_RET_OK)     //等待提示符
//        return RIL_ERROR;
	
	e->printf(e, "AT+NMGS=%d,%s\r\n",i->bufsize, i->buf);
		
    e->write(e, i->buf, i->bufsize);
    return (e->wait_resp(e, "OK", 5000) == AT_RET_OK) ? RIL_OK : RIL_ERROR;
    
}

/**  
 * @brief    数据接收处理
 */
static at_return socket_recv_handler(at_work_ctx_t *e)
{   
    int  datalen = 0;
    int  recvcnt = 0;
    char *start, *end;
    bool sync_header = false;                                    /* 接收头标志 */
    unsigned int timer;                                          /* 超时定时器 */    
    struct socket_info *i = (struct socket_info *)e->params;
    const socket_base_t  *s = i->s;
    /**
     * 命令格式
     *
     *    =>AT+QIRD=<connectID>[,<read_length>]
     *
     *    <= +QIRD: <read_actual_length><CR><LF><data>
     *    <= 
     *    <= OK              //结束符
     *   
     */
    //发送接收指令
    e->printf(e, "AT+QIRD=%d,%d",s->id, i->bufsize);
    timer = ril_get_ms();
    while (!ril_istimeout(timer, 3000 + i->bufsize)) {
        recvcnt += e->read(e, i->buf + recvcnt, i->bufsize - recvcnt);
        if (sync_header && recvcnt >= datalen) {
            e->wait_resp(e, "OK", 100);                         /* 等待结束符 */
            i->count = datalen;
            return AT_RET_OK;
        } else if (!sync_header && recvcnt >= 5) {
            datalen = 0;
            i->buf[recvcnt] = '\0';
            start = strstr((char *)i->buf, "+QIRD:");
            if (!start)
                continue;
            end = strstr(start, "\r\n");
            if (!end)
                continue;
            datalen = atoi(start + 6);                          /* 有效数据长度*/
            if (datalen) {
                RIL_DBG("%s\r\n", start);
                RIL_INFO("Recv %d data size.\r\n", datalen);                
            }
            /*将实际收到的数据移到数组前面 ------------------------------------*/
            recvcnt -= end + 2 - (char *)i->buf;              
            memmove(i->buf, end + 2, recvcnt); 
            sync_header = true;
        }
    }
    return AT_RET_TIMEOUT;
}


/**  
 * @brief    发送数据
 */
static int bc28_send(struct ril_device *dev, socket_base_t *s, 
                      const void *buf, unsigned int len)
{
    struct socket_info info = {s, (unsigned char *)buf, len, 0};
    if (len == 0)
        return RIL_REJECT;       
    return at_do_work(dev->at, (at_work)socket_send_handler, &info);
}

/**  
 * @brief    接收数据
 */
static unsigned int bc28_recv(struct ril_device *dev, socket_base_t *s,
                              void *buf, unsigned int max_recv_size)
{
    struct socket_info info = {s, (unsigned char *)buf, max_recv_size, 0};
    at_do_work(dev->at, (at_work)socket_recv_handler, &info);
    return info.count;
}

/**  
 * @brief    断开连接
 */
static int bc28_disconnect(struct ril_device *dev, socket_base_t *s)
{
    return ril_exec_cmdx(NULL, "AT+QLWSREGIND=1");
}


/**  
 * @brief    RIL请求
 */
static int bc28_request(struct ril_device *dev, ril_request_code num, void *data, 
                        int size)
{
    switch (num) {
    case RIL_REQ_GET_CONN_STATUS:
        return bc28_netconn_status(dev, (ril_netconn_status *)data);        
    default:
        return ril_request_default_proc(dev, num, data, size);
    }
}

/**
 * @brief  获取连接状态
 */
static sock_request_status bc28_conn_status(struct ril_device *dev, 
                                            socket_base_t *s)
{
//    int line_num;
//    char recv[100];    
//    char *lines[8];
//    int id, status;
//    
//    if (ril_exec_cmd( recv, sizeof(recv), "AT+QISTATE=1,%d",s->id) != RIL_OK)
//        return SOCK_STAT_UNKNOW;
//    line_num = at_split_respond_lines(recv, lines, 8, ',');
//    if (line_num < 6)
//        return SOCK_STAT_FAILED;
//    if (sscanf(recv, "%*[^+]+QISTATE: %d", &id) != 1 || id != s->id)
//        return SOCK_STAT_FAILED;
//    status = atoi(lines[5]);
//    
//    if (status == 2)
//        return SOCK_STAT_DONE;
//    else if (status == 4)
//        return SOCK_STAT_FAILED;
//    else 
//        return SOCK_STAT_BUSY;
	
	char recv[64];    
    char buff[20] = {0};
    if (ril_exec_cmd( recv, sizeof(recv), "AT+NMSTATUS?") != RIL_OK)
        return SOCK_STAT_FAILED;
    if (sscanf(recv, "%*[^+]+NMSTATUS:%s", buff) != 1)
        return SOCK_STAT_FAILED;  //#warning SOCK_STAT_UNKNOW

//	RIL_INFO("%s\r\n", buff);
    return strcmp(buff, "MO_DATA_ENABLED") ? SOCK_STAT_BUSY : SOCK_STAT_DONE;
}


/**
 * @brief 获取发送状态
 */
static sock_request_status bc28_send_status(struct ril_device *dev, socket_base_t *s)
{
	char recv[64];    
	unsigned int pending, sent, err;
    if (ril_exec_cmd(recv, sizeof(recv), "AT+NQMGS") != RIL_OK)
		return SOCK_STAT_FAILED;
    if (sscanf(recv, "%*[^P]PENDING=%d,SENT=%d,ERROR=%d", &pending, &sent, &err) != 3)
		return SOCK_STAT_FAILED;  //#warning SOCK_STAT_UNKNOW
	
    return ((err == 0) && (pending == 0)) ? SOCK_STAT_DONE : SOCK_STAT_BUSY;
}

/**
 * @brief   urc事件
 *          1. +QIURC: "closed",<connectID>   socket关闭
 *          2. +QIURC: "recv",<connectID>     收到数据
 *          3. +QIURC: "incoming full"        数据缓冲区满
 *          4. +QIURC: "pdpdeact",<contextID>  PDP关闭
 */
//static void tcp_urc_handler(at_urc_ctx_t *ctx)
//{
//    int line_num;
//    char *lines[2];
//    int id;
//    socket_base_t *s = NULL;
//    ril_netconn_status netconn = NETCONN_OFFLINE;
//    line_num = at_split_respond_lines(ctx->buf, lines, 2, ',');
//    if (line_num > 1) {
//        id = atoi(lines[1]);
//        s = find_socket_by_id(id);
//    }
//    if (strstr(ctx->buf, "closed") && s != NULL)
//        ril_socket_notify(s, SOCK_NOTFI_OFFLINE, NULL, 0);
//    else if (strstr(ctx->buf, "recv") && s != NULL)
//        ril_socket_notify(s, SOCK_NOTFI_DATA_INCOMMING, NULL, 0);
//    else if (strstr(ctx->buf, "pdpdeact"))
//        ril_notify(RIL_NOTIF_NETCONN, &netconn, sizeof(int *));
//    
//}ril_urc_register("+QIURC: ", tcp_urc_handler);


/**
 * @brief sim卡丢失事件
 */
//static void sim_urc_handler(at_urc_ctx_t *ctx)
//{
//    ril_sim_status sim = SIM_ABSENT;
//    ril_notify(RIL_NOTIF_SIM, &sim, sizeof(int *));
//}ril_urc_register("+CPIN: NOT READY", sim_urc_handler);



///**
// * @brief wait for the lwm2m observe
// */
//static void observe_urc_handler(at_urc_ctx_t *ctx)
//{
//    int evt;
//    if (sscanf(ctx->buf, "+QLWEVTIND:%d", &evt) == 1){        
//		switch (evt)
//		{
//			case 0:
//				RIL_INFO("Register completed\r\n");
//				break;
//			case 1:
//				RIL_INFO("Deregister completed\r\n");
//				break;
//			case 2:
//				RIL_INFO("Registration status updated\r\n");
//				break;
//			case 3:
//				RIL_INFO("Object 19/0/0 observe completed\r\n");

//				break;
//			default:
//				RIL_INFO("Other registration indication: %d\r\n", evt);
//				break;
//			
//		}
//    } 
//}ril_urc_register("+QLWEVTIND:", observe_urc_handler);


/**
 * @brief 执行下发命令
 */

static void cmdexe_urc_handler(at_urc_ctx_t *ctx)
{
    int evt;
    if (sscanf(ctx->buf, "+NNMI:%d", &evt) == 1){        
		switch (evt)
		{
			case 0:
				RIL_INFO("Register completed\r\n");
				break;
			case 1:
				RIL_INFO("Deregister completed\r\n");
				break;
			case 2:
				RIL_INFO("Registration status updated\r\n");
				break;
			case 3:
				RIL_INFO("Object 19/0/0 observe completed\r\n");
				break;
			default:
				RIL_INFO("Other registration indication: %d\r\n", evt);
				break;
			
		}
    } 
}ril_urc_register("+NNMI:", cmdexe_urc_handler);



/*BC28设备函数操作集 ----------------------------------------------------------*/
static const ril_device_ops_t bc28 = {
//	.reset           = bc28_reset,
    .init            = bc28_init,
    .shutdown        = bc28_shutdown,
    .pdp_setup       = bc28_pdp_setup,
    .pdp_contrl      = bc28_pdp_ctrl,
    .request         = bc28_request,
    .sock = {
        .connect     = bc28_sock_connect,
        .disconnect  = bc28_disconnect,
        .send        = bc28_send,
        .recv        = NULL,   //bc28_recv,
        .conn_status = bc28_conn_status,
        .send_status = bc28_send_status
    }
};


///* 安装BC28模组*/
//ril_device_install("bc28", bc28);

/* 安装BC35-G模组*/
ril_device_install("bc35-G", bc28);


