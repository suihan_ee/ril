/******************************************************************************
 * @brief    RIL 任务管理
 *
 * Copyright (c) 2024  <chenjunbin_ee@163.com>
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Change Logs: 
 * Date           Author       Notes 
 * 2024-06-05     Chenjunbin        Initial version
 ******************************************************************************/
#include "ril.h"
#include "ril_core.h"
#include "taskManager.h"
#include <stdio.h>
#include <string.h>

#include "ril_device.h"
#include "ril_socket.h"

#define  SOCKET_RX_BUF_SIZE     256

#pragma pack(1)
typedef struct
{
    int8_t messageId;
    int8_t Temperature;
    int8_t Humidity;
    uint16_t Luminance;
} tag_app_Agriculture;

typedef struct
{
    int8_t messageId;
    uint16_t mid;
    int8_t errcode;
    int8_t Light_State;
} tag_app_Response_Agriculture_Control_Light;

typedef struct
{
    int8_t messageId;
    uint16_t mid;
    int8_t errcode;
    int8_t Motor_State;
} tag_app_Response_Agriculture_Control_Motor;

typedef struct
{
    int8_t messageId;
    uint16_t mid;
    char Light[3];
} tag_app_Agriculture_Control_Light;

typedef struct
{
    uint8_t messageId;
    uint16_t mid;
    char Motor[3];
} tag_app_Agriculture_Control_Motor;
#pragma pack()

#define cn_app_Agriculture 0x0
#define cn_app_Agriculture_Control_Light 0x1
#define cn_app_response_Agriculture_Control_Light 0x2
#define cn_app_Agriculture_Control_Motor 0x3
#define cn_app_response_Agriculture_Control_Motor 0x4




static ril_socket_t socket_id;




static void socket_event_handler(ril_socket_t s, socket_event_type type)
{
	if (s == socket_id) {
		switch (type) {
			case SOCK_EVENT_CONN:
				RIL_INFO("Trigger SOCK_EVENT_CONN\r\n");
				break;
			case SOCK_EVENT_SEND:
				RIL_INFO("Trigger SOCK_EVENT_SEND\r\n");
				break;
			case SOCK_EVENT_RECV:
				RIL_INFO("Trigger SOCK_EVENT_RECV\r\n");
				break;
			default:
				RIL_ERR("socket event type: %d is unknow!\r\n", type);
				break;
		}
	
	} else 
		RIL_ERR("Ril socket id: %ld is unknow!\r\n", s);
}

/**
 * @brief wait for the lwm2m observe
 */
static void observe_urc_handler(at_urc_ctx_t *ctx)
{
    int evt;
    if (sscanf(ctx->buf, "+QLWEVTIND:%d", &evt) == 1){        
		switch (evt)
		{
			case 0:
				RIL_INFO("Register completed\r\n");
				break;
			case 1:
				RIL_INFO("Deregister completed\r\n");
				break;
			case 2:
				RIL_INFO("Registration status updated\r\n");
				break;
			case 3:
				RIL_INFO("Object 19/0/0 observe completed\r\n");
				break;
			default:
				RIL_INFO("Other registration indication: %d\r\n", evt);
				break;
			
		}
    } 
}ril_urc_register("+QLWEVTIND:", observe_urc_handler);


//make a byte to 2 ascii hex
static int byte_to_hexstr(const unsigned char *bufin, int len, char *bufout)
{
    int i = 0;
    if (NULL == bufin || len <= 0 || NULL == bufout)
    {
        return -1;
    }
    for(i = 0; i < len; i++)
    {
        (void) sprintf(bufout+i*2, "%02X", bufin[i]);
    }
    return 0;
}



static char sendbuf[128];

#define IA1_DATA_FRAME "{ msg_id: %d, temp: %d, humi: %d, Lum: %d}" 

static void lwm2m_agriculture_task(void)
{
	tag_app_Agriculture Agriculture;
//	uint16_t length;
	ril_obj_t * ril_obj_pt;
	bool flag = false;
	
	ril_obj_pt = get_ril_obj();
	
	socket_id = ril_sock_create(socket_event_handler, SOCKET_RX_BUF_SIZE);
	
	while (1) {
		
		ril_delay(5000);
		
		RIL_INFO("ril_obj.status.conn:%d\r\n", ril_obj_pt->status.conn);
		if ((ril_obj_pt->status.conn == NETCONN_ONLINE) && (!flag)) {
		                            
			flag = true;
			ril_sock_connect(socket_id, "221.229.214.202", 5683, RIL_SOCK_UDP);
		} else if (!flag)
			continue;
	
		Agriculture.messageId = cn_app_Agriculture;
		Agriculture.Temperature = 32;
        Agriculture.Humidity = 40;
        Agriculture.Luminance = 31;
		
		(void)memset(sendbuf, 0, sizeof(sendbuf));
		sprintf(sendbuf, IA1_DATA_FRAME, Agriculture.messageId, Agriculture.Temperature, Agriculture.Humidity, Agriculture.Luminance);
	
			
//		byte_to_hexstr((unsigned char *)&Agriculture, sizeof(tag_app_Agriculture), sendbuf);
//		if (ril_sock_online(socket_id)) {
			//发送数据
		RIL_INFO("Start to send data!\r\n");
		ril_sock_send(socket_id, sendbuf, strlen(sendbuf));
		
//		} else {
//			//注册平台
//		
//		}
		
	}

}
task_define("agriculture", lwm2m_agriculture_task, 256, 8);
